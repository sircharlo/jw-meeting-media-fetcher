# JW Meeting Media Fetcher

Welcome! Here is a script that facilitates the downloading of media for personal study when access to [JW.org](https://www.jw.org) or JW Library is limited, or for playback during midweek meetings.

This is a Node.js script, and can be run as a normal user.

## Prerequisites

The prerequisites are simple enough. You need an installation of Node.js, as well as the the following npm modules:

* better-sqlite3
* chalk
* glob
* graceful-fs
* md5-file
* minimist
* moment
* rimraf
* trash
* urllib-sync
* zip-local


## Usage

Running the script with no parameters displays the following help:
    
    
    USAGE:
      jw-meeting-media-fetcher.js -l L -s path [-d YYYYMMDD] [--debug]

    Required parameters:
      -l L            Where 'L' is the language code used on the JW.org website.
                      For example, 'E' for English, 'U' for Russian, and so on.
    
      -s path         Where 'path' is the folder where media files should be saved.
    
    Optional parameters:
      -d YYYY-MM-DD   Where 'YYYY-MM-DD' is the date to be used for JWPUB retrieval.
                      This basedate is used get that and the following month’s media.
                      By default, this is set as today’s date.
          


## Does this infringe the JW.org Terms of Use?

No, the JW.org [Terms of Use](https://www.jw.org/en/terms-of-use) actually *explicitly allow* this. Here is the relevant excerpt (emphasis mine):

>You may not:
> 
> Create for distribution purposes, any software applications, tools, or techniques that are specifically made to collect, copy, download, extract, harvest, or scrape data, HTML, images, or text from this site. **(This does *not* prohibit the distribution of free, non-commercial applications designed to download electronic files such as EPUB, PDF, MP3, and MP4 files from public areas of this site.)**

## Help!

If ever you run into any issues with the script, please feel free to let me know.

*- COS*
