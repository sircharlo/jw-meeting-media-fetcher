/*jshint esversion: 6, node: true */

// Pre-reqs
const chalk = require("chalk");
const glob = require("glob");
const fs = require("graceful-fs");
const md5 = require("md5-file");
const args = require("minimist")(process.argv.slice(2));
const moment = require("moment");
const path = require("path");
const rimraf = require("rimraf");
const request = require("urllib-sync").request;
const zipper = require("zip-local");
const reqTimeout = 10000;

const log = console.log;
const error = chalk.red;
const bold = chalk.bold;
const info = chalk.blue;
const pathColor = chalk.blueBright;
const localColor = chalk.cyan;
const remoteColor = chalk.yellow;
const ul = chalk.underline;
const param = chalk.green;
const option = chalk.greenBright;
const paramOptional = chalk.magenta;
const optionOptional = chalk.magentaBright;

var perf = {};
perf.start = Date.now();
perf.downloads = {
  downloadFile: [],
  downloadJwpub: [],
  downloadJwpubJson: [],
  localMediaItem: [],
  updateSongs: []
};
perf.downloadCounter = 0;

function status(type, message, padding) {
  var string = "[ " + type + " ]";
  if (!padding) {
    padding = 2;
  }
  string = (" ").repeat(padding) + string.padEnd(20, " ");
  if (type.includes("DOWNLOAD")) {
    string = remoteColor(bold(string));
  } else if (type.includes("IDENTICAL")) {
    string = localColor(bold(string));
  } else if (type.includes("ERROR")) {
    string = error(bold(string));
    message = error(message);
  } else if (type.includes("PERF")) {
    string = param(bold(string));
  } else {
    string = info(bold(string));
  }
  log(string + " " + message);
}

function aliasUpdate() {
  var congs = glob.sync(congPath + "/*");
  var toAlias = glob.sync(mediaPath + "/*/*/*");
  for (var alias of toAlias) {
    for (var cong of congs) {
      fs.symlink(alias, cong + alias.replace(glob.sync(mediaPath), ""), function(err) {
        if (typeof err === 'object' && err !== null && err.code && err.code !== "EEXIST") {
          log(err);
        }
      });
    }
  }
  var curAliases = glob.sync(congPath + "/*/*/*/*");
  for (var curAlias of curAliases) {
    var stat = fs.lstatSync(curAlias);
    if (stat.isSymbolicLink() && !fs.existsSync(fs.readlinkSync(curAlias))) {
      rimraf.sync(curAlias, {}, function(err) {
        log(error(err));
      });
    }
  }
}

function downloadFile(url, destpath, filename) {
  status("DOWNLOAD", remoteColor(filename), 4);
  var res;
  try {
    res = request(url, {
      timeout: 3600000
    });
    perf.downloads.downloadFile.push(url);
    mkdirSync(destpath);
    var savePath = destpath + "/" + filename;
    fs.writeFileSync(savePath, res.data);
    var localChecksum = md5.sync(savePath);
    fs.writeFileSync(savePath + ".md5", localChecksum);
    status("MD5", remoteColor(filename + ".md5") + " saved.", 4);
  } catch (error) {
    status(res.status + " ERROR", "[ " + remoteColor(url) + " ]");
  }
}

function downloadJwpub(UndatedSymbol, IssueTagNumber, type) {
  var refDocJsonUrl = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&fileformat=JWPUB&pub=" + UndatedSymbol + "&langwritten=" + args.l + "&issue=" + IssueTagNumber;
  try {
    var docRefFileName, docRefFile = null;
    if (!perf.downloads.downloadJwpubJson.includes(refDocJsonUrl)) {
      var docRefJsonReq = request(refDocJsonUrl, {
        timeout: reqTimeout
      });
      if (docRefJsonReq.status >= 399) {
        throw docRefJsonReq.status;
      }
      var docRefJson = docRefJsonReq.data.toString();
      perf.downloads.downloadJwpubJson.push(refDocJsonUrl);
      docRefFile = JSON.parse(docRefJson).files[args.l].JWPUB[0].file;
      docRefFileName = docRefFile.url.split("/").pop();
    }
    unzipPaths.currentJwpub = pubsPath + "/" + UndatedSymbol;
    mkdirSync(unzipPaths.currentJwpub);
    unzipPaths.currentJwpub = unzipPaths.currentJwpub + "/" + IssueTagNumber;
    mkdirSync(unzipPaths.currentJwpub);
    if (type == "main") {
      unzipPaths.currentMwb = unzipPaths.currentJwpub;
    }
    var docRefDest = unzipPaths.currentJwpub + "/" + docRefFileName;
    if (docRefFile !== null && !perf.downloads.downloadJwpub.includes(docRefFile.url) && !identicalLocalRemote(docRefDest, docRefFile)) {
      downloadFile(docRefFile.url, unzipPaths.currentJwpub, docRefFileName);
      perf.downloads.downloadJwpub.push(docRefFile.url);
      zipper.sync.unzip(docRefDest).save(unzipPaths.currentJwpub);
      mkdirSync(unzipPaths.currentJwpub + "/contents-unzipped");
      zipper.sync.unzip(unzipPaths.currentJwpub + "/contents").save(unzipPaths.currentJwpub + "/contents-unzipped");
      var unzippedFiles = glob.sync(unzipPaths.currentJwpub + "/contents-unzipped/*");
      for (var f = 0; f < unzippedFiles.length; f++) {
        var localChecksum = md5.sync(unzippedFiles[f]);
        fs.writeFileSync(unzippedFiles[f] + ".md5", localChecksum);
      }
    } else {
      //status("IDENTICAL", remoteColor(docRefFileName))
    }
    var refDbFile = glob.sync(unzipPaths.currentJwpub + "/contents-unzipped/*.db")[0];
    return require('better-sqlite3')(refDbFile);
  } catch (error) {
    status(error + " ERROR", "[ " + remoteColor(refDocJsonUrl) + " ]");
    return false;
  }
}

function identicalLocalRemote(local, remote) {
  if (typeof remote !== 'object') {
    var oldRemote = remote;
    remote = {};
    remote.url = oldRemote;
    if (fs.existsSync(remote.url + ".md5")) {
      remote.checksum = fs.readFileSync(remote.url + ".md5").toString();
    } else {
      remote.checksum = md5.sync(remote.url);
      fs.writeFileSync(remote.url + ".md5", remote.checksum);
      status("MD5", remoteColor(remote.url + ".md5") + " saved.", 4);
    }
  }
  var result = false,
    localChecksum;
  if (!alreadyCheckedForNewerVersion[local + remote.url]) {
    if (fs.existsSync(local)) {
      if (fs.existsSync(local + ".md5")) {
        localChecksum = fs.readFileSync(local + ".md5").toString();
      } else {
        localChecksum = md5.sync(local);
        fs.writeFileSync(local + ".md5", localChecksum);
        status("MD5", remoteColor(local + ".md5") + " saved.", 4);
      }
      if (remote.checksum === localChecksum) {
        result = true;
      }
    }
    alreadyCheckedForNewerVersion[local + remote.url] = result;
  }
  result = false && log(result, localChecksum, remote.checksum);
  return alreadyCheckedForNewerVersion[local + remote.url];
}

function invalidParams() {
  var procName = bold(chalk.yellow(process.argv[1].split("/").pop())),
    usage = "";
  usage += ul("\nUSAGE:") + "\n  " + procName + " " + param("-l ") + option("L ") + param("-s ") + option("path ") + paramOptional("[-d ") + optionOptional("YYYYMMDD") + paramOptional("] ") + "\n\n";
  usage += ul(param(bold("Required parameters:\n")));
  usage += "  " + param("-l ") + option("L") + "            Where " + option("'L'") + " is the language code used on the JW.org website.\n";
  usage += "                  For example, " + option("'E'") + " for English, " + option("'U'") + " for Russian, and so on.\n\n";
  usage += "  " + param("-s ") + option("path") + "         Where " + option("'path'") + " is the folder where media files should be saved.\n\n";
  usage += ul(optionOptional("Optional parameters:\n"));
  usage += "  " + paramOptional("-d ") + optionOptional("YYYY-MM-DD") + "   Where " + optionOptional("'YYYY-MM-DD'") + " is the date to be used for JWPUB retrieval.\n";
  usage += "                  This basedate is used get that and the following month's media.\n";
  usage += "                  By default, this is set as today's date.\n\n";
  log(usage);
  process.exit();
}

function mediaMW() {
  var wbDates = [baseDate, baseDate.clone().add(1, "months")];
  for (var wbDate = 0; wbDate < wbDates.length; wbDate++) { // WB for this month and next month
    status("MW", "Retrieving the " + wbDates[wbDate].format("MMMM YYYY") + " Meeting Workbook...");
    var db = downloadJwpub("mwb", wbDates[wbDate].format("YYYYMM") + "00", "main");
    for (var yP of glob.sync(congPath + "/*/").concat(mediaPath)) {

      mkdirSync(yP + "/" + wbDates[wbDate].format("YYYY"));
    }

    var weeks = {};
    var weekQuery = db.prepare("SELECT FirstDateOffset FROM DatedText").all();
    for (w = 0; w < weekQuery.length; w++) {
      weeks[weekQuery[w].FirstDateOffset] = {};
    }
    for (var week in weeks) {
      if (moment(week, "YYYYMMDD").isSameOrAfter(baseDate) && moment(week, "YYYYMMDD").isBefore(baseDate.clone().add(weeksToFetch, "weeks"))) {
        var FirstDateOffset = week,
          sortOrder = {},
          videos = [],
          weekDisplay = moment(week, "YYYYMMDD").format("YYYY-MM-DD"),
          weekPath = "";

        for (var wP of glob.sync(congPath + "/*/").concat(mediaPath)) {
          weekPath = wP + "/" + wbDates[wbDate].format("YYYY") + "/" + weekDisplay;
          mkdirSync(weekPath);
        }
        status(weekDisplay, "Updating media files...");

        // add th cover
        var refThCover = {};
        refThCover.ImagePath = glob.sync(pubsPath + "/th/*th*over*")[0];
        refThCover.ImageName = path.basename(refThCover.ImagePath);
        refThCover.DestPath = weekPath + "/" + refThCover.ImageName;
        if (!identicalLocalRemote(refThCover.DestPath, refThCover.ImagePath)) {
          fs.copyFile(refThCover.ImagePath, refThCover.DestPath, (err) => {
            if (err) throw err;
          });
          status("Ref - th", localColor(refThCover.ImageName), 4);
        }

        var startDocId = db.prepare("SELECT DocumentId FROM DatedText WHERE FirstDateOffset=" + FirstDateOffset + "").get().DocumentId;
        var lastDocId = db.prepare("SELECT DocumentId FROM DatedText WHERE DocumentId>" + startDocId + " ORDER BY DatedTextId ASC LIMIT 1").get();
        if (!lastDocId) {
          lastDocId = db.prepare("SELECT DocumentId FROM Document ORDER BY DocumentId DESC LIMIT 1").get().DocumentId;
        } else {
          lastDocId = lastDocId.DocumentId;
          lastDocId = lastDocId - 1;
        }
//      var localMediaItems = db.prepare("SELECT DocumentMultimedia.DocumentId,DocumentMultimedia.MultimediaId,DocumentMultimedia.BeginParagraphOrdinal,Multimedia.MultimediaId,Multimedia.KeySymbol,Multimedia.MimeType,Multimedia.MepsDocumentId,Multimedia.FilePath,Multimedia.Track,Multimedia.IssueTagNumber,Multimedia.Label,Multimedia.Caption FROM DocumentMultimedia INNER JOIN Multimedia ON Multimedia.MultimediaId = DocumentMultimedia.MultimediaId WHERE DocumentMultimedia.DocumentId >= " + startDocId + " AND DocumentMultimedia.DocumentId <= " + lastDocId + " AND (((Multimedia.MimeType='video/mp4' OR Multimedia.MimeType='audio/mpeg') AND NOT Multimedia.KeySymbol='th') OR (Multimedia.MimeType='image/jpeg' AND Multimedia.CategoryType <> 9) OR (MepsDocumentId))").all();
      var localMediaItems = db.prepare("SELECT DocumentMultimedia.DocumentId,DocumentMultimedia.MultimediaId,DocumentMultimedia.BeginParagraphOrdinal,Multimedia.MultimediaId,Multimedia.KeySymbol,Multimedia.MimeType,Multimedia.MepsDocumentId,Multimedia.FilePath,Multimedia.Track,Multimedia.IssueTagNumber,Multimedia.Label,Multimedia.Caption FROM DocumentMultimedia INNER JOIN Multimedia ON Multimedia.MultimediaId = DocumentMultimedia.MultimediaId WHERE DocumentMultimedia.DocumentId >= " + startDocId + " AND DocumentMultimedia.DocumentId <= " + lastDocId + " AND (((Multimedia.MimeType='video/mp4' OR Multimedia.MimeType='audio/mpeg') AND NOT Multimedia.KeySymbol='sjj' AND NOT Multimedia.KeySymbol='th') OR (Multimedia.MimeType='image/jpeg' AND Multimedia.CategoryType <> 9) OR (MepsDocumentId))").all();
//log(localMediaItems)
        for (var i = 0; i < localMediaItems.length; i++) {
          var localMediaItem = localMediaItems[i];
          if (!sortOrder[localMediaItem.DocumentId]) {
            sortOrder[localMediaItem.DocumentId] = [];
          }
          if (!sortOrder[localMediaItem.DocumentId].includes(localMediaItem.BeginParagraphOrdinal)) {
            sortOrder[localMediaItem.DocumentId].push(localMediaItem.BeginParagraphOrdinal);
          }
          localMediaItem.FileType = localMediaItem.FilePath.split(".").pop();
          if (localMediaItem.MimeType.includes("audio") || localMediaItem.MimeType.includes("video")) {
            if (localMediaItem.KeySymbol == null) {
              localMediaItem.JsonUrl = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&docid=" + localMediaItem.MepsDocumentId + "&langwritten=" + args.l;
            } else {
              localMediaItem.JsonUrl = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&pub=" + localMediaItem.KeySymbol + "&langwritten=" + args.l + "&issue=" + localMediaItem.IssueTagNumber + "&track=" + localMediaItem.Track;
            }
            if (localMediaItem.MimeType.includes("video")) {
              localMediaItem.JsonUrl = localMediaItem.JsonUrl + "&fileformat=MP4";
              videos.push(localMediaItem.JsonUrl);
            } else if (localMediaItem.MimeType.includes("audio")) {
              localMediaItem.JsonUrl = localMediaItem.JsonUrl + "&fileformat=MP3";
            }
            try {
              var localMediaJsonReq = request(localMediaItem.JsonUrl, {
                timeout: reqTimeout
              });

              if (localMediaJsonReq.status >= 399) {
                throw localMediaJsonReq.status;
              }
              var localMediaJson = localMediaJsonReq.data.toString();

              perf.downloads.localMediaItem.push(localMediaItem.JsonUrl);
              var mediaFile = Object.values(JSON.parse(localMediaJson).files[args.l])[0].pop();
              localMediaItem.Label = mediaFile.title;
              localMediaItem.File = mediaFile.file;
              localMediaItem.FileName = localMediaItem.File.url.split("/").pop();
              localMediaItem.Type = "Remote";
            } catch (error) {
              localMediaItem.Code = error;
              localMediaItem.Type = "error";
            }
          } else {
            if (localMediaItem.Label.length == 0 && localMediaItem.Caption.length == 0) {
              localMediaItem.FileName = "Media";
            } else if (localMediaItem.Label.length > localMediaItem.Caption.length) {
              localMediaItem.FileName = localMediaItem.Label;
            } else {
              localMediaItem.FileName = localMediaItem.Caption;
            }
            localMediaItem.FileName = localMediaItem.FileName + "." + localMediaItem.FilePath.split(".").pop();
            localMediaItem.LocalPath = unzipPaths.currentMwb + "/contents-unzipped/" + localMediaItem.FilePath;
            localMediaItem.Type = "Local";
          }

          if (localMediaItem.Type !== "error") {
            localMediaItem.FileName = sanitizeFilename("0." + (localMediaItem.MimeType.includes("video") ? videos.length : Object.keys(sortOrder).length).toString().padStart(2, '0') + ".mwb." + parseInt(sortOrder[localMediaItem.DocumentId].indexOf(localMediaItem.BeginParagraphOrdinal) + 1).toString().padStart(2, '0') + /*localMediaItem.BeginParagraphOrdinal + "- " +*/ " " + localMediaItem.FileName);
            localMediaItem.DestPath = weekPath + "/" + localMediaItem.FileName;
            if (localMediaItem.Type == "Remote") {
              if (!identicalLocalRemote(localMediaItem.DestPath, localMediaItem.File)) {
                downloadFile(localMediaItem.File.url, weekPath, localMediaItem.FileName);
              } else {
                //status("IDENTICAL", remoteColor(localMediaItem.FileName))
              }
            } else if (localMediaItem.Type == "Local") {
              if (!identicalLocalRemote(localMediaItem.DestPath, localMediaItem.LocalPath)) {
                fs.copyFile(localMediaItem.LocalPath, localMediaItem.DestPath, (err) => {
                  if (err) throw err;
                });
                status(localMediaItem.Type, localColor(localMediaItem.FileName), 4);
              }
            }
          } else {
            status(localMediaItem.Code + " ERROR", "[ " + remoteColor(localMediaItem.JsonUrl) + " ]");
          }
        }

//        var weekRefDocs = db.prepare("SELECT DocumentExtract.BeginParagraphOrdinal,DocumentExtract.DocumentId,Extract.RefPublicationId,Extract.RefMepsDocumentId,Extract.RefPublicationId,Extract.RefMepsDocumentId,UndatedSymbol,IssueTagNumber FROM DocumentExtract INNER JOIN Extract ON DocumentExtract.ExtractId = Extract.ExtractId INNER JOIN RefPublication ON Extract.RefPublicationId = RefPublication.RefPublicationId WHERE DocumentExtract.DocumentId >= " + startDocId + " AND DocumentExtract.DocumentId <=" + lastDocId + " AND NOT UndatedSymbol = \"mwbr\" ORDER BY DocumentExtract.BeginParagraphOrdinal").all();
       var weekRefDocs = db.prepare("SELECT DocumentExtract.BeginParagraphOrdinal,DocumentExtract.DocumentId,Extract.RefPublicationId,Extract.RefMepsDocumentId,Extract.RefPublicationId,Extract.RefMepsDocumentId,UndatedSymbol,IssueTagNumber FROM DocumentExtract INNER JOIN Extract ON DocumentExtract.ExtractId = Extract.ExtractId INNER JOIN RefPublication ON Extract.RefPublicationId = RefPublication.RefPublicationId WHERE DocumentExtract.DocumentId >= " + startDocId + " AND DocumentExtract.DocumentId <=" + lastDocId + " AND NOT UndatedSymbol = \"sjj\" AND NOT UndatedSymbol = \"mwbr\" ORDER BY DocumentExtract.BeginParagraphOrdinal").all();
//log(weekRefDocs)
        weekRefDocs = weekRefDocs.reduce(function(accumulator, current) {
          if (checkIfAlreadyExist(current)) {
            return accumulator;
          } else {
            return accumulator.concat([current]);
          }

          function checkIfAlreadyExist(currentVal) {
            return accumulator.some(function(item) {
              return (item.UndatedSymbol === currentVal.UndatedSymbol && item.IssueTagNumber === currentVal.IssueTagNumber && item.RefMepsDocumentId === currentVal.RefMepsDocumentId);
            });
          }
        }, []);

        var paragraphs = [];
        for (var weekRefDoc = 0; weekRefDoc < weekRefDocs.length; weekRefDoc++) {
          var refDoc = weekRefDocs[weekRefDoc];
//log(refDoc.UndatedSymbol)
          if ((refDoc.UndatedSymbol.replace(/[0-9]/g, '') !== "w" && refDoc.UndatedSymbol.replace(/[0-9]/g, '') !== "g") || parseInt(refDoc.IssueTagNumber) >= 20000000) {
            if (refDoc.UndatedSymbol == "w" && parseInt(refDoc.IssueTagNumber) >= 20080801 && refDoc.IssueTagNumber.slice(-2) == "01") {
              refDoc.UndatedSymbol = "wp";
            }
            var refDb = downloadJwpub(refDoc.UndatedSymbol, refDoc.IssueTagNumber, "ref");
            // hacky bit for th brochure covers here...
            if (refDoc.UndatedSymbol == "th") {
              var refThChapter = refDb.prepare("SELECT ChapterNumber FROM Document WHERE MepsDocumentId=" + refDoc.RefMepsDocumentId).get().ChapterNumber.toString().padStart(2, '0');
              var refThTitle = {};
              refThTitle.ImagePath = glob.sync(pubsPath + "/th/*th*" + refThChapter + "*")[0];
              refThTitle.ImageName = path.basename(refThTitle.ImagePath);
              refThTitle.DestPath = weekPath + "/" + refThTitle.ImageName;
              if (!identicalLocalRemote(refThTitle.DestPath, refThTitle.ImagePath)) {
                fs.copyFile(refThTitle.ImagePath, refThTitle.DestPath, (err) => {
                  if (err) throw err;
                });
                status("Ref - " + refDoc.UndatedSymbol, localColor(refThTitle.ImageName), 4);
              }
            } else if (refDb) { // end of hacky th bit
              var refDocumentId = refDb.prepare("SELECT DocumentId FROM Document WHERE MepsDocumentId=" + refDoc.RefMepsDocumentId).get().DocumentId;
              var refDMExists = Object.values(refDb.prepare("SELECT EXISTS (SELECT * FROM sqlite_master WHERE type='table' AND name='DocumentMultimedia')").get())[0];
              var refMediaTable = "Multimedia";
              if (refDMExists == 1) {
                refMediaTable = "DocumentMultimedia";
              }
              var refMediaLookups = refDb.prepare("SELECT MultimediaId FROM " + refMediaTable + " a WHERE DocumentId = " + refDocumentId + " AND a.MultimediaId in (select b.MultimediaId from Multimedia b WHERE (CategoryType <> 9 AND CategoryType > 0))").all();
              for (var refMediaLookup = 0; refMediaLookup < refMediaLookups.length; refMediaLookup++) {
                var refMedia = refDb.prepare("SELECT Label,Caption,FilePath FROM Multimedia WHERE MultimediaId = " + refMediaLookups[refMediaLookup].MultimediaId).get();
                if (refMedia.Label.length > refMedia.Caption.length) {
                  refMedia.Title = refMedia.Label;
                } else {
                  refMedia.Title = refMedia.Caption;
                }
                refMedia.FileExt = refMedia.FilePath.split(".").pop();
                if (!paragraphs.includes(refDoc.BeginParagraphOrdinal)) {
                  paragraphs.push(refDoc.BeginParagraphOrdinal);
                }
                refMedia.FileName = sanitizeFilename(paragraphs.length + "." + refDoc.BeginParagraphOrdinal.toString().padStart(2, '0') + "." + refDoc.UndatedSymbol + "." + (parseInt(refMediaLookup) + 1).toString().padStart(2, '0') + " " + refMedia.Title + "." + refMedia.FileExt);
                refMedia.LocalPath = unzipPaths.currentJwpub + "/contents-unzipped/" + refMedia.FilePath;
                refMedia.DestPath = weekPath + "/" + refMedia.FileName;
                if (!identicalLocalRemote(refMedia.DestPath, refMedia.LocalPath)) {
                  fs.copyFile(refMedia.LocalPath, refMedia.DestPath, (err) => {
                    if (err) throw err;
                  });
                  status("Ref - " + refDoc.UndatedSymbol, localColor(refMedia.FileName), 4);
                }
              }
            }
          }
        }
      }
    }
    log(" ");
  }
}

function mediaWE() {
  var wtBaseDate = baseDate;
  var wtDates = [wtBaseDate.clone().subtract(2, "months"), wtBaseDate.clone().subtract(1, "months")];
  for (var wtDate = 0; wtDate < wtDates.length; wtDate++) { // WT for this month and next month
    status("WT", "Retrieving the " + wtDates[wtDate].format("MMMM YYYY") + " Watchtower...");
    var db = downloadJwpub("w", wtDates[wtDate].format("YYYYMM") + "00", "main");
    var weeks = [];
    var weekQuery = db.prepare("SELECT FirstDateOffset FROM DatedText").all();
    for (w = 0; w < weekQuery.length; w++) {
      weeks.push(weekQuery[w].FirstDateOffset);
    }
    var documents = db.prepare("SELECT Document.DocumentId FROM Document WHERE Document.Class=40").all();

    for (var w = 0; w < weeks.length; w++) {
      if (moment(weeks[w], "YYYYMMDD").isSameOrAfter(wtBaseDate) && moment(weeks[w], "YYYYMMDD").isBefore(wtBaseDate.clone().add(weeksToFetch, "weeks"))) {
        var week = moment(weeks[w], "YYYYMMDD").add(6, "days").format("YYYY-MM-DD"),
          weekPath = "";
        for (var yP of glob.sync(congPath + "/*/").concat(mediaPath)) {

          var yearPath = yP + "/" + moment(week, "YYYYMMDD").format("YYYY");
          mkdirSync(yP + "/" + moment(week, "YYYYMMDD").format("YYYY"));
          weekPath = yearPath + "/" + week;
          mkdirSync(weekPath);
        }
        status(week, "Updating media files...");
        var localMediaItems = db.prepare("SELECT DocumentMultimedia.MultimediaId,Document.DocumentId,Multimedia.CategoryType,Multimedia.FilePath,Label,Caption FROM DocumentMultimedia INNER JOIN Document ON Document.DocumentId = DocumentMultimedia.DocumentId INNER JOIN Multimedia ON DocumentMultimedia.MultimediaId = Multimedia.MultimediaId WHERE Document.DocumentId = " + documents[w].DocumentId + " AND Multimedia.CategoryType <> 9").all();
        var songRefs = db.prepare("SELECT * FROM Extract INNER JOIN DocumentExtract ON DocumentExtract.ExtractId = Extract.ExtractId WHERE DocumentId = " + documents[w].DocumentId + " and Caption LIKE '%sjj%'").all();
        for (var song = 0; song < songRefs.length; song++) {
          var songNum = songRefs[song].Caption.replace(/\D/g,"");
          var songFileName= "sjjm_U_" + songNum.toString().padStart(3, '0') + "_r720P.mp4";
          var songPath = songsPath + "/" + songFileName;
          songFileName = song.toString().padEnd(3, '0') + "_" + songFileName;
          var weekSongPath = weekPath + "/" + songFileName;
          if (!identicalLocalRemote(weekSongPath, songPath)) {
            fs.copyFile(songPath, weekSongPath, (err) => {
              if (err) throw err;
            });
            status("Local song", localColor(songFileName), 4);
          }
        }
        for (var i = 0; i < localMediaItems.length; i++) {
          var localMediaItem = localMediaItems[i];
          //log(localMediaItems)
          localMediaItem.FileType = localMediaItem.FilePath.split(".").pop();
          if (localMediaItem.Label.length == 0 && localMediaItem.Caption.length == 0) {
            localMediaItem.FileName = "Media";
          } else if (localMediaItem.Label.length > localMediaItem.Caption.length) {
            localMediaItem.FileName = localMediaItem.Label;
          } else {
            localMediaItem.FileName = localMediaItem.Caption;
          }
          localMediaItem.FileName = localMediaItem.FileName + "." + localMediaItem.FilePath.split(".").pop();
          localMediaItem.LocalPath = unzipPaths.currentMwb + "/contents-unzipped/" + localMediaItem.FilePath;
          localMediaItem.Type = "Local";
          localMediaItem.FileName = sanitizeFilename((parseInt(i) + 1).toString().padStart(2, '0') + " " + localMediaItem.FileName);
          localMediaItem.DestPath = weekPath + "/" + localMediaItem.FileName;
          if (!identicalLocalRemote(localMediaItem.DestPath, localMediaItem.LocalPath)) {
            fs.copyFile(localMediaItem.LocalPath, localMediaItem.DestPath, (err) => {
              if (err) throw err;
            });
            status(localMediaItem.Type, localColor(localMediaItem.FileName), 4);
          }
        }
      }
    }
    log(" ");
  }
}

function mkdirSync(dirPath) {
  try {
    fs.mkdirSync(dirPath);
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }
}

function sanitizeFilename(filename) {
  filename = filename.replace(/[?!"»«\(\)\\\[\]№—\$]*/g, "").replace(/[;:,|/]+/g, " - ").replace(/ +/g, " ").replace(/\.+/g, ".").replace(/\r?\n/g, " - ");
  var bytes = Buffer.byteLength(filename, 'utf8');
  var toolong = 230;
  if (bytes > toolong) {
    var fe = filename.split(".").pop();
    var chunks = filename.split(" - ");
    while (bytes > toolong) {
      if (chunks.length > 1) {
        chunks.pop();
        filename = chunks.join(" - ");
      } else {
        filename = filename.substring(0, 90);
        chunks = [filename];
      }
      bytes = Buffer.byteLength(filename + "." + fe, 'utf8');
    }
    filename = chunks.join(" - ") + "." + fe;
    bytes = Buffer.byteLength(filename, 'utf8');
  }
  return filename;
}

function updateSongs() {
  var songs = {},
    songTypes = ["MP4", "MP3"];
  for (var songType = 0; songType < songTypes.length; songType++) {
    var filetype = songTypes[songType];
    status("SONGS", "Checking for updated " + filetype + " songs...");
    songs[filetype] = {};
    songs[filetype].JsonUrl = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&pub=sjjm&fileformat=" + filetype + "&langwritten=" + args.l;
    songs[filetype].NewJson = JSON.parse(request(songs[filetype].JsonUrl, {
      timeout: reqTimeout
    }).data.toString());
    perf.downloads.updateSongs.push(songs[filetype].JsonUrl);
    for (var songNum = 0; songNum < songs[filetype].NewJson.files[args.l][filetype].length; songNum++) {
      var song = songs[filetype].NewJson.files[args.l][filetype][songNum];
      if ((filetype == "MP4" && song.label == "720p") || (filetype == "MP3" && song.track > 0)) {
        var filename = song.file.url.split("/").pop();
        if (!identicalLocalRemote(songsPath + "/" + filename, song.file)) {
          downloadFile(song.file.url, songsPath, filename);
        }
      }
    }
  }
}

if (!args.l || !args.s) {
  invalidParams();
}

// User variables
var w = 0,
  alreadyCheckedForNewerVersion = {},
  unzipPaths = {},
  outputPath = glob.sync(args.s + "/" + args.l)[0],
  pubsPath = glob.sync(outputPath + "/Publications")[0],
  mediaPath = glob.sync(outputPath + "/Media")[0],
  congPath = glob.sync(outputPath + "/Congregations")[0],
  songsPath = glob.sync(outputPath + "/Songs")[0],
  deleteDate = moment(baseDate).subtract(1, "month"),
  baseDate = moment(args.d).startOf('isoWeek'),
  weeksToFetch = 2; // number of weeks to fetch; set to 2 unless theres some forecast downtime..

for (var nP of [outputPath, pubsPath, mediaPath, congPath, songsPath]) {
  mkdirSync(nP);
}

log(" ");

log(info("     ██╗██╗    ██╗") + "    ███╗   ███╗███████╗███████╗████████╗██╗███╗   ██╗ ██████╗                    ");
log(info("     ██║██║    ██║") + "    ████╗ ████║██╔════╝██╔════╝╚══██╔══╝██║████╗  ██║██╔════╝                    ");
log(info("     ██║██║ █╗ ██║") + "    ██╔████╔██║█████╗  █████╗     ██║   ██║██╔██╗ ██║██║  ███╗                   ");
log(info("██   ██║██║███╗██║") + "    ██║╚██╔╝██║██╔══╝  ██╔══╝     ██║   ██║██║╚██╗██║██║   ██║                   ");
log(info("╚█████╔╝╚███╔███╔╝") + "    ██║ ╚═╝ ██║███████╗███████╗   ██║   ██║██║ ╚████║╚██████╔╝                   ");
log(info(" ╚════╝  ╚══╝╚══╝ ") + "    ╚═╝     ╚═╝╚══════╝╚══════╝   ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝                    ");
log("                                                                                                   ");
log("███╗   ███╗███████╗██████╗ ██╗ █████╗     ███████╗███████╗████████╗ ██████╗██╗  ██╗███████╗██████╗ ");
log("████╗ ████║██╔════╝██╔══██╗██║██╔══██╗    ██╔════╝██╔════╝╚══██╔══╝██╔════╝██║  ██║██╔════╝██╔══██╗");
log("██╔████╔██║█████╗  ██║  ██║██║███████║    █████╗  █████╗     ██║   ██║     ███████║█████╗  ██████╔╝");
log("██║╚██╔╝██║██╔══╝  ██║  ██║██║██╔══██║    ██╔══╝  ██╔══╝     ██║   ██║     ██╔══██║██╔══╝  ██╔══██╗");
log("██║ ╚═╝ ██║███████╗██████╔╝██║██║  ██║    ██║     ███████╗   ██║   ╚██████╗██║  ██║███████╗██║  ██║");
log("╚═╝     ╚═╝╚══════╝╚═════╝ ╚═╝╚═╝  ╚═╝    ╚═╝     ╚══════╝   ╚═╝    ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝");

if (!args.d) {
  log(" ");
  for (var deletePath of [mediaPath, congPath + "/*/"]) {
    var dP = deletePath + "/" + deleteDate.format("YYYY") + "/" + deleteDate.format("YYYY-MM") + "*";
    status("CLEANUP", "Cleaning up: '" + pathColor(dP) + "'");
    rimraf.sync(dP, {}, function(err) {
      log(error(err));
    });
  }
}

log(" ");

perf.songsStart = Date.now();
updateSongs();
perf.songsEnd = Date.now();

log(" ");

perf.weStart = Date.now();
mediaWE();
perf.weEnd = Date.now();

perf.mwStart = Date.now();
mediaMW();
perf.mwEnd = Date.now();

log(" ");

perf.aliasStart = Date.now();
aliasUpdate();
perf.aliasEnd = Date.now();

if (perf.songsStart) {
  status("PERFORMANCE", localColor("Songs update") + " completed in " + param(moment.utc(moment.duration(perf.songsEnd - perf.songsStart).asMilliseconds()).format("mm:ss.SSS")) + ".");
}

if (perf.weStart) {
  status("PERFORMANCE", localColor("WE meeting media update") + " completed in " + param(moment.utc(moment.duration(perf.weEnd - perf.weStart).asMilliseconds()).format("mm:ss.SSS")) + ".");
}

if (perf.mwStart) {
  status("PERFORMANCE", localColor("MW meeting media update") + " completed in " + param(moment.utc(moment.duration(perf.mwEnd - perf.mwStart).asMilliseconds()).format("mm:ss.SSS")) + ".");
}

if (perf.aliasStart) {
  status("PERFORMANCE", localColor("Multi-congregation mirror update") + " completed in " + param(moment.utc(moment.duration(perf.aliasEnd - perf.aliasStart).asMilliseconds()).format("mm:ss.SSS")) + ".");
}

const vals = Object.values(perf.downloads);
for (const val of vals) {
  perf.downloadCounter += val.length;
}

log(" ");
status("INFO", "Number of requests: " + remoteColor(perf.downloadCounter));

/*log(" ");
log(perf.downloads);
log(" ");*/

log(" ");
perf.end = Date.now();
status("PERFORMANCE", localColor("Program execution") + " completed in " + param(moment.utc(moment.duration(perf.end - perf.start).asMilliseconds()).format("mm:ss.SSS")) + ".");
